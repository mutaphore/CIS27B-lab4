
public class SudCell {

	boolean digits[];
	private boolean completed;
	
	static final int MIN_VAL = 1;
	static final int MAX_VAL = 9;
	static final int NUM_VALS = 9;
	static final int SQRT_NUM_VALS = 3;
	static final int NOT_FINAL_YET = -1;
	
	//Constructor---------------------------
	public SudCell() {
		
		digits = new boolean[NUM_VALS];
		
		for (int i=0 ; i<NUM_VALS ; i++) 
			digits[i] =  true;
		
		completed = false;	
		
	}
	
	public String ToString() {
		
		int NumTrue = 0;
		int index = -1;
		
		for (int i=0 ; i<NUM_VALS ; i++) {
			if(digits[i] == true) {
				NumTrue++;
				index = i;
			}
		}
		
		if(NumTrue == 1)
			return " " + Integer.toString(index+1) + " ";
		else
			return " - ";
		
	}
	
	public String ToStringReveal() {
		
		StringBuffer TempBuffer = new StringBuffer();
		
		TempBuffer.append(" ");
		
		for (int i=0 ; i<NUM_VALS ; i++) 
			if(digits[i] == true)
				TempBuffer.append(i+1);
			else if(IsEmptyCell()) {
				TempBuffer.append("-");
				i = NUM_VALS;
			}

		TempBuffer.append(" ");
		
		return TempBuffer.toString();
		
	}
	
	//Accessors------------------------------------- 
	
	public int GetFinalValue() {
		
		int NumTrue = 0;
		int index = -1;
		
		for (int i=0 ; i<NUM_VALS ; i++) {
	
			if(digits[i] == true) {
				NumTrue++;
				index = i;
			}
	
		}
		
		if(NumTrue == 1)
			return index+1;
		else
			return NOT_FINAL_YET;
		
	}
	
	public boolean IsCompleted() {
		
		return completed;
		
	}
	
	public boolean ValuePresent(int val) {
		
		if(!Validate(val))
			return false;
		
		return digits[val-1];
		
	}
	
	public int NumRemainingVals() {
		
		int count = 0;
		
		for (int i=0 ; i<NUM_VALS ; i++)
			if(digits[i] == true)
				count++;
		
		return count;
		
	}
	
	public boolean IsEmptyCell() {
		
		for (int i=0 ; i<NUM_VALS ; i++)
			if(digits[i] == true)
				return false;
			
		return true;
		
	}
	
	//Mutators-------------------------------------
	
	public void SetToBlank() {
		
		for (int i=0 ; i<NUM_VALS ; i++)
			digits[i] = true;
		
	}
	
	public boolean ForcedSet(int val) {
	
		if(!Validate(val))
			return false;
		
		for (int i=0 ; i<NUM_VALS ; i++) {
		
			if(i == val-1)
				digits[i] = true;
			else
				digits[i] = false;
		
		}
		
		return true;
	
	}
	
	public boolean Complete() {
		
		int NumTrue = 0;
		
		for (int i=0 ; i<NUM_VALS ; i++) {
			if(digits[i] == true)
				NumTrue++;
		}
		
		if(NumTrue == 1) {
			completed = true;
			return true;
		}
		else
			return false;
		
	}
	
	public boolean RuleOut(int val) {
		
		if(!Validate(val))
			return false;
		
		digits[val-1] = false;
		
		return true;
		
	}
	
	public boolean SoftSet(int val) {
		
		if(!Validate(val))
			return false;
		
		digits[val-1] = true;
		
		return true;
		
	}
	
	//Helpers----------------------------------
	
	static private boolean Validate(int val) {
		
		if(val > MAX_VAL || val < MIN_VAL)
			return false;
		else
			return true;
		
	}
	
}
