
public class Foothill {

	public static void main(String[] args) {
		
		//Test SudCell Class----------------------------------
		
		SudCell sud1, sud2, sud3;
      
      sud1 = new SudCell();
      sud2 = new SudCell();
      sud3 = new SudCell();
      
      System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<Start testing SudCell Class>>>>>>>>>>>>>>>>>>>>>>>>\n");
      
      //Test RuleOut()
      System.out.println(sud1.RuleOut(3));
      System.out.println(sud1.RuleOut(4));
      System.out.println(sud2.ForcedSet(9));
      System.out.println(sud3.RuleOut(0));
      System.out.println(sud3.RuleOut(11));
      System.out.println(sud3.RuleOut(4));
      System.out.println(sud3.RuleOut(6));
      
      //Test ToString() and ToStringReveal()
      System.out.println("\n" + sud1.ToStringReveal());
      System.out.println(sud2.ToString());
      System.out.println(sud2.ToStringReveal());
      System.out.println(sud3.ToString());
      System.out.println(sud3.ToStringReveal() + "\n");
      
      //Test GetFinalValue()
      System.out.println("The final value of sud1 is: " + sud1.GetFinalValue());
      System.out.println("The final value of sud2 is: " + sud2.GetFinalValue());
      System.out.println("The final value of sud3 is: " + sud3.GetFinalValue() + "\n");
      
      //Test Complete() and IsCompleted()
      System.out.println("Is sud 2 completed?: " + sud2.IsCompleted());
      sud2.Complete();
      System.out.println("Is sud 2 completed after calling Complete()?: " + sud2.IsCompleted() + "\n");
      
      //Test ValuePresent()
      System.out.println(sud3.ValuePresent(11));
      System.out.println(sud3.ValuePresent(4));
      System.out.println(sud3.ValuePresent(2));
      System.out.println(sud3.ValuePresent(6) + "\n");
      
      //Test NumRemainingVals()
      System.out.println("How many values left at sud1?: " + sud1.NumRemainingVals());
      System.out.println("How many values left at sud2?: " + sud2.NumRemainingVals() + "\n");
      
      //Test IsEmptyCell()
      System.out.println("Is sud2 an empty cell?: " + sud2.IsEmptyCell());
      sud2.RuleOut(9);
      System.out.println("Is sud2 an empty cell after ruling out the last value?: " + sud2.IsEmptyCell() +"\n");

      //Test SetToBlank()
      sud1.SetToBlank();
      System.out.println("How many values remaining after set sud1 to blank?:" + sud1.NumRemainingVals() + "\n");
      
      //Test SoftSet()
      sud3.SoftSet(4);
      System.out.println("After softset 4 back into sud3, here are the values remaining: " + 
      		sud3.ToStringReveal());
      sud3.SoftSet(5);
      System.out.println("After softset 5 (a value already turned on) to sud3: " + 
      		sud3.ToStringReveal() + "\n");
  
      
      //Test Sudoku Class----------------------------------
      
      Sudoku puzzle;

      puzzle = new Sudoku();
      
      System.out.println("<<<<<<<<<<<<<<<<<<<<<<<<<<Start testing Sudoku Class>>>>>>>>>>>>>>>>>>>>>>>>\n");
      
      // set some values in the matrix, attempting illegal
      puzzle.ForcedSet(3, 4, 5);
      puzzle.ForcedSet(7, 1, 3);
      puzzle.ForcedSet(9, 2, 6);
      for (int k = -1; k < 12; k++)
         if ( !puzzle.ForcedSet(k, k, k) )
            System.out.println( "Could not set " + k);

      System.out.print( "\n ------------- first basic ------------- \n");
      puzzle.Display(false);

      System.out.print( "\n ------------- then revealed ------------- \n");
      puzzle.Display(true);
      System.out.print( "\n" );;

      for (int row = Sudoku.MIN_VAL; row <= Sudoku.MAX_VAL ; row++)
         for (int col = Sudoku.MIN_VAL; col <= Sudoku.MAX_VAL ; col++)
         {
            // rule out 2 through 8 in all places except middle row and col
            if ( row == 5 || col == 5 )
               continue;
             for (int k = 2; k <= 8; k++)
               puzzle.RuleOut(row, col, k);
         }

      System.out.print( "\n ---------- after massive rule-outs ---------- \n");
      puzzle.Display(true);
      
      //Test SetToBlank()
      puzzle.SetToBlank();
      System.out.print( "\n ---------- after set to blank ---------- \n");
      puzzle.Display(true);
      
      //Test NumRemainingValsTotal()
      System.out.println( "\nNumber of remaining values to be solved is: " + puzzle.NumRemainingValsTotal());
      
      //Test Complete() and IsCompleted() and ValuePresent()
      for(int i = 2; i <= 9; i++)
      	puzzle.RuleOut(1, 1, i);
      puzzle.Complete(1, 1);
      System.out.println( "\nIs (1,1) completed after ruling out 1-8? " + puzzle.IsCompleted(1, 1));
      System.out.println( "\nAt (1,1) is 5 still present after ruling out 1-8? " + puzzle.ValuePresent(1, 1, 5));
      
      for(int i = 2; i <= 6; i++)
      	puzzle.RuleOut(2, 2, i);
      System.out.println( "\nAt (2,2) number of remaining values after ruling out 2-6? " + puzzle.NumRemainingVals(2, 2));
      
      //Set all cells to completed with singleton number 1
      for (int row = Sudoku.MIN_VAL; row <= Sudoku.MAX_VAL ; row++) {
         for (int col = Sudoku.MIN_VAL; col <= Sudoku.MAX_VAL ; col++) {
         	for (int k = 2; k <= 9; k++) {
               puzzle.RuleOut(row, col, k);
         	}
         	puzzle.Complete(row, col);
         }
      }  	
      
      System.out.println( "\nAll singletons after completing all cells with 1?: " + puzzle.AllSingletons());
      System.out.print( "\n ---------- after set all to 1 ---------- \n");
      puzzle.Display(true);      

      //Rule out 2-9 in (3,3) to create an empty cell
      for (int k = 2; k <= 9; k++) 
         puzzle.RuleOut(3, 3, k);
      
      System.out.println( "\nIs there an empty cell after ruling out 2-9 at (3,3)? " + puzzle.EmptyCellFound());
      
      //Rule out 1-9 in (3,3) to create an empty cell
      for (int k = 1; k <= 9; k++) 
         puzzle.RuleOut(3, 3, k);
      
      System.out.println( "\nIs there an empty cell after ruling out 1-9 at (3,3)? " + puzzle.EmptyCellFound());
      System.out.print( "\n ---------- after creating an empty cell at (3,3) ---------- \n");
      puzzle.Display(true);
      
      puzzle.SoftSet(4, 4, 5);
      System.out.print( "\n ---------- after softset 5 into cell at (4,4) ---------- \n");
      puzzle.Display(true);
      
      
      
	}

}
