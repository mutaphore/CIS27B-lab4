
public class Sudoku {

	private SudCell suds[][];
	static final int MIN_VAL = SudCell.MIN_VAL;
	static final int MAX_VAL = SudCell.MAX_VAL;
	static final int NUM_VALS = SudCell.NUM_VALS;
	static final int SQRT_NUM_VALS = SudCell.SQRT_NUM_VALS;
	//Constant to indicate array row or column entered is out of the defined array size
	static final int OUT_OF_BOUNDS = -999; 
	
	//Constructor
	public Sudoku() {
		
		AllocateSudCells();
		
	}
	
	//Displays Sudoku boxes
	public void Display(boolean reveal) {
		
		if(reveal == false) {
			
			for (int i=0 ; i<NUM_VALS ; i++) {
				
				for (int j=0 ; j<NUM_VALS ; j++) {
					
					System.out.print(suds[i][j].ToString());
					if(j == 2 || j == 5)
						System.out.print("|");
				}
			
				System.out.print("\n");
				if(i == 2 || i == 5 )
					System.out.print("-----------------------------\n");
			}
			
		}
		else {
			
			for (int i=0 ; i<NUM_VALS ; i++) {
				
				for (int j=0 ; j<NUM_VALS ; j++) {
					
					System.out.print(suds[i][j].ToStringReveal());
					if(j == 2 || j == 5)
						System.out.print("|");
				}
			
				System.out.print("\n");
				if(i == 2 || i == 5 )
					System.out.print("-----------------------------\n");
			}
		
		}
		
	}
	
	//Accessors---------------------------------------------
	
	public int GetFinalValue(int row, int col) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1))
			return suds[row-1][col-1].GetFinalValue();
		else
			return OUT_OF_BOUNDS;
		
	}
	
	public boolean IsCompleted(int row, int col) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1))
			return suds[row-1][col-1].IsCompleted();
		else
			return false;
		
	}
	
	public boolean ValuePresent(int row, int col, int val) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1) && Validate(val))
			return suds[row-1][col-1].ValuePresent(val);
		else
			return false;
		
	}
	
	public int NumRemainingVals(int row, int col) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1))
			return suds[row-1][col-1].NumRemainingVals();
		else
			return OUT_OF_BOUNDS;
		
	}
	
	public int NumRemainingValsTotal() {
		
		int count=0;
		
		for(int i=1 ; i<=NUM_VALS ; i++)
			for(int j=1 ; j<=NUM_VALS ; j++)
				count+=NumRemainingVals(i,j);
		
		return count;
		
	}
	
	public boolean AllSingletons() {
		
		boolean allSingletons = true;
		
		for(int i=0 ; i<NUM_VALS ; i++)
			for(int j=0 ; j<NUM_VALS ; j++)
				if(!suds[i][j].IsCompleted())
					allSingletons = false;
		
		return allSingletons;
		
	}
	
	public boolean EmptyCellFound() {
		
		for(int i=0 ; i<NUM_VALS ; i++)
			for(int j=0 ; j<NUM_VALS ; j++)
				if(suds[i][j].IsEmptyCell())
					return true;
		
		return false;
		
	}
	
	//Mutators----------------------------------------------
	
	public void SetToBlank() {
		
		for(int i=0 ; i<NUM_VALS ; i++)
			for(int j=0 ; j<NUM_VALS ; j++)
				suds[i][j].SetToBlank();
		
	}
	
	public boolean ForcedSet(int row, int col, int val) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1) && Validate(val)) {
			return suds[row-1][col-1].ForcedSet(val);
		}
		else
			return false;
		
	}
	
	public boolean Complete(int row, int col) {
	
		if(IndexValidate(row-1) && IndexValidate(col-1)) {
			return suds[row-1][col-1].Complete();
		}
		else
			return false;
		
	}
	
	public boolean RuleOut(int row, int col, int val) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1) && Validate(val)) {
			return suds[row-1][col-1].RuleOut(val);
		}
		else
			return false;
		
	}
	
	public boolean SoftSet(int row, int col, int val) {
		
		if(IndexValidate(row-1) && IndexValidate(col-1) && Validate(val)) {
			return suds[row-1][col-1].SoftSet(val);
		}
		else
			return false;
		
	}
	
	
	//Helpers-----------------------------
	private boolean Validate (int val) {
		
		if(MIN_VAL <= val && val <= MAX_VAL)
			return true;
		else
			return false;
		
	}
	
	private boolean IndexValidate(int n) {
		
		if(0 <= n && n <= NUM_VALS-1)
			return true;
		else
			return false;
		
	}
	
	private void AllocateSudCells() {
		
		suds = new SudCell[NUM_VALS][NUM_VALS];
		
		for (int i=0 ; i<NUM_VALS ; i++)
			for (int j=0 ; j<NUM_VALS ; j++)
				suds[i][j] = new SudCell();
	
	}
	
}
